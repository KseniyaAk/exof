﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModulesLibrary;

namespace ClassLibraryEO
{
    class CalculateRateService
    {
        public static void CalculateRate(Rate rate, int changedNumber)
        {
            switch (changedNumber)
            {
                case 2:
                    {
                        rate.rateFrom = rate.rateTo * 0.9;
                        break;
                    }
                case 3:
                    {                        
                        rate.rateTo = rate.rateFrom * 1.10;
                        break;
                    }
                default: break;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModulesLibrary;
using System.IO;

namespace ClassLibraryEO
{
    public static class BillService
    {
        private static Int64 operationNumber;       
        private const String path = @"D:\1 университет\5 семестр\Прога\Проект\ExchangeOffice\ClassLibraryEO\bill{0}.doc";
        //private const String readPathPicture = @"D:\1 университет\5 семестр\Прога\Проект\ExchangeOffice\ClassLibraryEO\element.doc";



        public static void WriteBill(User user, Transaction transaction)
        {
            operationNumber=transaction.operationNumber;
            using (StreamWriter sw = new StreamWriter(String.Format(path, operationNumber), false, System.Text.Encoding.Default))
            {
                sw.WriteLine("Обменный пункт Банка \"Наталия & Ксения. Лучшие курсы страны\"");
                sw.WriteLine();
               /* String picture = "";
                using (StreamReader sr = new StreamReader(readPathPicture, System.Text.Encoding.Default))
                {
                    picture = sr.ReadToEnd();
                }
                sw.WriteLine(picture);
                sw.WriteLine();*/
                sw.WriteLine("Имя: {0}", user.name);
                sw.WriteLine("Фамилия: {0}", user.surname);
                sw.WriteLine("Дата : {0}", transaction.dateTime.Date);
                sw.WriteLine("Время : {0}", transaction.dateTime.TimeOfDay);
                sw.WriteLine("Обен валюты с {0} объёмом {1} на {2} объёмом {3} по курсу {4}", 
                    transaction.fromCurrency, transaction.amountFromCurrency, transaction.toCurrency, transaction.amountToCurrency, transaction.rate);
                sw.WriteLine("Номер операции: {0}", operationNumber);
                sw.WriteLine();
                sw.WriteLine("Приходите ещё. Мы всегда вам рады");
            }
        }
    
        public static String GetFileName()
        {
            return String.Format(path, operationNumber);
        }        
    }
}

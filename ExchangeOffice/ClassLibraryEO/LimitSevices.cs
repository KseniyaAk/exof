﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModulesLibrary;
using DataBaseAccess;

namespace ClassLibraryEO
{
    public static class LimitSevices
    {
        public static Boolean CheckLimit(Double amountFrom, Double amountTo, Double commonFrom, Double commonTo,  Double limitFrom, Double limitTo)
        {
            if (amountFrom + commonFrom > limitFrom || amountFrom + commonTo > limitTo) return false;
            else return true;            
        }        
    }
}

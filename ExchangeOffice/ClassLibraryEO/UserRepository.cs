﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModulesLibrary;
using DataBaseAccess;
using System.Collections;

namespace ClassLibraryEO
{
    public class UserRepository: IEnumerable
    {
        private List<User> users { get; set; }

        internal UserRepository()
        {
            users = UsersAccess.GetData(); 
        }

        public User Find(String id)
        {
            Updata();
            if (users.Any(u => u.id == id)) return (from u in users where u.id == id select u).First();
            else throw new UserNotFoundException();            
        }

        public User Find(String name, String surname)
        {
            Updata();
            if ( users.Any(u => (u.name == name && u.surname == surname)) ) return (from u in users select u).First();
            return null;
        }
        public Boolean Contain(String id)
        {
            Updata();
            if (users.Any(u => u.id == id)) return true;
            return false;
        }
        internal void Add(User user)
        {
            UsersAccess.Add(user);
            users.Add(user);
            Updata();
        }
        public void Updata()
        {
            users = UsersAccess.GetData();
        }
        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable<User>)users).GetEnumerator();
        }

    }
}

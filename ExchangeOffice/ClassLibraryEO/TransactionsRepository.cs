﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModulesLibrary;
using DataBaseAccess;
using System.Collections;

namespace ClassLibraryEO
{
    public class TransactionsRepository: IEnumerable
    {
        private List<Transaction> transactions { get; set; }        
        
        internal TransactionsRepository()
        {
            transactions = TransactionsAccess.GetData();            
        }

        //FILTERS
        public void FilterByFromCurrency(string cur)
        {
            transactions=transactions.FindAll(x=> x.fromCurrency == cur) ;
            
        }

        public void FilterByToCurrency(string cur)
        {
            transactions = transactions.FindAll(x => x.toCurrency == cur);
        }

        public void FilterByDate(DateTime from, DateTime to)
        {
            transactions = transactions.FindAll(x => (x.dateTime.Date >= from) && (x.dateTime.Date <= to));
            
        }

        public void FilterById(string id)
        {
            transactions = transactions.FindAll(x => x.id == id);
        }

        //CALCULATIONS
        public Double CalculateVolumeFrom(String fromCurrency)
        {
            Double amount = 0;
            foreach (var tr in transactions)
            {
                if (tr.fromCurrency == fromCurrency) amount += tr.amountFromCurrency;
            }
            return amount;            
        }

        public Double CalculateVolumeTo(String toCurrency)
        {
            Double amount = 0;
            foreach (var tr in transactions)
            {
                if (tr.toCurrency == toCurrency) amount += tr.amountToCurrency;
            }
            return amount;
        }

        public Double CalculateVolumeFrom()
        {
            Double amount = 0;
            foreach (var tr in transactions)
            {
                amount += tr.amountFromCurrency;
            }
            return amount;
        }

        public Double CalculateVolumeTo()
        {
            Double amount = 0;
            foreach (var tr in transactions)
            {
                amount += tr.amountToCurrency;
            }
            return amount;
        }

        /*public Double CalculateVolumeFrom(String fromCurrency, String id)
        {
            Double amount = 0;
            foreach (var tr in transactions)
            {
                if (tr.id == id && tr.fromCurrency == fromCurrency) amount += tr.amountFromCurrency;
            }
            return amount;

        }

        public Double CalculateVolumeTo(String toCurrency, String id)
        {
            Double amount = 0;
            foreach (var tr in transactions)
            {
                if (tr.id == id && tr.toCurrency == toCurrency) amount += tr.amountToCurrency;
            }
            return amount;
        }*/


        /*public Boolean LimitCheck(Transaction ntr, Double limitFrom, Double limitTo)
        {
            Double amount = ntr.amountFromCurrency;
            foreach (var tr in transactions)
            {
                if (tr.id == ntr.id && tr.fromCurrency==ntr.fromCurrency) amount += tr.amountFromCurrency;
            }
            if (amount >= limitFrom) return false;
            else
            {
                amount = ntr.amountToCurrency;
                foreach (var tr in transactions)
                {
                    if (tr.id == ntr.id && tr.toCurrency == ntr.toCurrency) amount += tr.amountToCurrency;
                }
                if (amount >=limitTo) return false;
                return true;
            }
                
        }*/
        public void ResetFilters()
        {
            transactions = TransactionsAccess.GetData();
            transactions = TransactionsAccess.GetData();
        }

        internal void Add(Transaction transaction)
        {
            if (transactions.Count == 0)
            {
                transaction.operationNumber = 1;
            }
            else transaction.operationNumber = transactions.Last().operationNumber + 1;            
            TransactionsAccess.Add(transaction);
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable<Transaction>)transactions).GetEnumerator();
        }
        
    }
}

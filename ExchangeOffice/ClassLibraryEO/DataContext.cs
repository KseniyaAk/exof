﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModulesLibrary;


namespace ClassLibraryEO
{
    public class DataContext
    {
        public RateRepository rates { get; private set; }
        public TransactionsRepository transactions { get; private set; }
        public UserRepository users { get; private set; }
        public LimitRepository limits { get; private set; }

        public DataContext()
        {
            rates = new RateRepository();
            transactions = new TransactionsRepository();
            users = new UserRepository();
            limits = new LimitRepository();
        }

        //RATES
        public void Add(Rate newRate, Boolean CalculateRate, int changedNumber)
        {
            if (newRate.nameFromCurrency == newRate.nameToCurrency) throw new FormatException();
            if (!CalculateRate)
            {
                if (newRate.rateTo <= 0 || newRate.rateFrom <= 0) throw new FormatException();
            }
            else
            {
                if ((newRate.rateTo <= 0 && newRate.rateTo != -1) || (newRate.rateFrom <= 0 && newRate.rateFrom != -1)) throw new FormatException();
                CalculateRateService.CalculateRate(newRate, changedNumber);

            }
            
            if(rates.Contain(newRate.nameFromCurrency, newRate.nameToCurrency))
            {
                throw new ExchangeRateAlreadyExistException();
            }
            
            rates.Add(newRate);
        }
        public void Add(Rate newRate, int index, Boolean CalculateRate, int changedNumber)
        {
            if (!CalculateRate)
            {
                if (newRate.rateTo <= 0 || newRate.rateFrom <= 0) throw new FormatException();
            }
            else
            {
                if ((newRate.rateTo <= 0 && newRate.rateTo != -1) || (newRate.rateFrom <= 0 && newRate.rateFrom != -1)) throw new FormatException();
                CalculateRateService.CalculateRate(newRate, changedNumber);
            }
            
            /*if (rates.Contain(newRate.nameFromCurrency, newRate.nameToCurrency))
            {
                throw new ExchangeRateAlreadyExistException();
            }*/

            rates.Add(newRate, index);
        }

        //TRANSACTIONS
        public void Add(Transaction newTransaction)
        {
            try
            {
                if (!LimitSevices.CheckLimit(newTransaction.amountFromCurrency, newTransaction.amountToCurrency, 
                    transactions.CalculateVolumeFrom(newTransaction.fromCurrency), transactions.CalculateVolumeFrom(newTransaction.toCurrency), 
                    limits.Find(newTransaction.fromCurrency), limits.Find(newTransaction.toCurrency)))
                {
                    throw new LimitExcessExeption();
                }
            }
            catch (LimitNotFoundException) { throw new LimitNotFoundException(); }
            transactions.Add(newTransaction);
        }

        //USERS
        public void Add(User newUser)
        {
            if( users.Contain(newUser.id)  )
            {
                throw new UserAlreadyExistExeption();
            }
            try
            {
                if (IsRightFormat(newUser.name) && IsRightFormat(newUser.surname)) users.Add(newUser);
            }
            catch (FormatException) { throw new FormatException(); }
            
        }

        //LIMITS
        public void Set(Limit limit)
        {
            if (limit.limit <= 0) throw new FormatException();
            Boolean isCurExist=false;
            foreach(var c in rates.GetCurrencies())
            {
                if (c.Equals(limit.nameOfCurrency))
                {
                    isCurExist = true;
                    break;
                }
            }
            if (!isCurExist) throw new LimitNotFoundException();
            limits.Add(limit);

        }

        public Boolean IsRightFormat(String name)
        {
            foreach(var se in name)
            {
                if (!((se >= 'a' && se <= 'z') || (se >= 'A' && se <= 'Z')))
                {
                    foreach (var sr in name)
                    {
                        if (!((sr >= 'а' && sr <= 'я') || (sr >= 'А' && sr <= 'Я'))) throw new FormatException();
                        
                    }
                    return true;                     
                            
                }
                    
            }
            return true;
        }
       /* public void SavChangese(List<Int32> deleteRows)
        {
            rates.Save(deleteRows);
        }

        public List<String> GetCurrencies()
        {
            return rates.GetCurrencies();
        }
        */
    }
}

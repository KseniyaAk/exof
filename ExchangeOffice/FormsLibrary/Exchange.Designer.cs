﻿namespace FormsLibrary
{
    partial class Exchange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExSurname = new System.Windows.Forms.TextBox();
            this.ExName = new System.Windows.Forms.TextBox();
            this.ExId = new System.Windows.Forms.TextBox();
            this.ExAmount = new System.Windows.Forms.TextBox();
            this.ExExchange = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Amount = new System.Windows.Forms.Label();
            this.ExFrom = new System.Windows.Forms.ComboBox();
            this.ExTo = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ExSurname
            // 
            this.ExSurname.Enabled = false;
            this.ExSurname.Location = new System.Drawing.Point(73, 90);
            this.ExSurname.Name = "ExSurname";
            this.ExSurname.Size = new System.Drawing.Size(142, 20);
            this.ExSurname.TabIndex = 0;
            // 
            // ExName
            // 
            this.ExName.Enabled = false;
            this.ExName.Location = new System.Drawing.Point(73, 45);
            this.ExName.Name = "ExName";
            this.ExName.Size = new System.Drawing.Size(142, 20);
            this.ExName.TabIndex = 1;
            // 
            // ExId
            // 
            this.ExId.Location = new System.Drawing.Point(73, 8);
            this.ExId.Name = "ExId";
            this.ExId.Size = new System.Drawing.Size(142, 20);
            this.ExId.TabIndex = 2;
            this.ExId.TextChanged += new System.EventHandler(this.ExId_TextChanged);
            // 
            // ExAmount
            // 
            this.ExAmount.Location = new System.Drawing.Point(73, 170);
            this.ExAmount.Name = "ExAmount";
            this.ExAmount.Size = new System.Drawing.Size(142, 20);
            this.ExAmount.TabIndex = 5;
            // 
            // ExExchange
            // 
            this.ExExchange.BackColor = System.Drawing.Color.Lavender;
            this.ExExchange.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ExExchange.Location = new System.Drawing.Point(0, 208);
            this.ExExchange.Name = "ExExchange";
            this.ExExchange.Size = new System.Drawing.Size(262, 28);
            this.ExExchange.TabIndex = 6;
            this.ExExchange.Text = "Exchange";
            this.ExExchange.UseVisualStyleBackColor = false;
            this.ExExchange.Click += new System.EventHandler(this.button_Exchange_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Surname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "From";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(156, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "To";
            // 
            // Amount
            // 
            this.Amount.AutoSize = true;
            this.Amount.Location = new System.Drawing.Point(8, 177);
            this.Amount.Name = "Amount";
            this.Amount.Size = new System.Drawing.Size(43, 13);
            this.Amount.TabIndex = 12;
            this.Amount.Text = "Amount";
            // 
            // ExFrom
            // 
            this.ExFrom.FormattingEnabled = true;
            this.ExFrom.Items.AddRange(new object[] {
            "",
            "BYN",
            "EURO",
            "USD"});
            this.ExFrom.Location = new System.Drawing.Point(73, 129);
            this.ExFrom.Name = "ExFrom";
            this.ExFrom.Size = new System.Drawing.Size(72, 21);
            this.ExFrom.TabIndex = 13;
            this.ExFrom.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // ExTo
            // 
            this.ExTo.FormattingEnabled = true;
            this.ExTo.Items.AddRange(new object[] {
            "",
            "BYN",
            "EURO",
            "USD"});
            this.ExTo.Location = new System.Drawing.Point(183, 128);
            this.ExTo.Name = "ExTo";
            this.ExTo.Size = new System.Drawing.Size(75, 21);
            this.ExTo.TabIndex = 14;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.ExExchange);
            this.panel1.Controls.Add(this.ExTo);
            this.panel1.Controls.Add(this.ExSurname);
            this.panel1.Controls.Add(this.ExFrom);
            this.panel1.Controls.Add(this.ExName);
            this.panel1.Controls.Add(this.Amount);
            this.panel1.Controls.Add(this.ExId);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.ExAmount);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(8, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(262, 236);
            this.panel1.TabIndex = 15;
            // 
            // Exchange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(284, 257);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(300, 296);
            this.Name = "Exchange";
            this.Text = "Exchange";
            this.Load += new System.EventHandler(this.Exchange_Load_1);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox ExSurname;
        private System.Windows.Forms.TextBox ExName;
        private System.Windows.Forms.TextBox ExId;
        private System.Windows.Forms.TextBox ExAmount;
        private System.Windows.Forms.Button ExExchange;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Amount;
        private System.Windows.Forms.ComboBox ExFrom;
        private System.Windows.Forms.ComboBox ExTo;
        private System.Windows.Forms.Panel panel1;
        
    }
}
﻿namespace FormsLibrary
{
    partial class Bill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bill));
            this.Print = new System.Windows.Forms.Button();
            this.printDocumentBill = new System.Drawing.Printing.PrintDocument();
            this.printDialogBill = new System.Windows.Forms.PrintDialog();
            this.printPreviewDialogBill = new System.Windows.Forms.PrintPreviewDialog();
            this.printPreviewControlBill = new System.Windows.Forms.PrintPreviewControl();
            this.SuspendLayout();
            // 
            // Print
            // 
            this.Print.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Print.BackColor = System.Drawing.Color.Lavender;
            this.Print.Location = new System.Drawing.Point(672, 386);
            this.Print.Name = "Print";
            this.Print.Size = new System.Drawing.Size(75, 23);
            this.Print.TabIndex = 1;
            this.Print.Text = "Print";
            this.Print.UseVisualStyleBackColor = false;
            this.Print.Click += new System.EventHandler(this.Print_Click);
            // 
            // printDialogBill
            // 
            this.printDialogBill.AllowSelection = true;
            this.printDialogBill.AllowSomePages = true;
            this.printDialogBill.PrintToFile = true;
            this.printDialogBill.ShowHelp = true;
            this.printDialogBill.UseEXDialog = true;
            // 
            // printPreviewDialogBill
            // 
            this.printPreviewDialogBill.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialogBill.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialogBill.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialogBill.Enabled = true;
            this.printPreviewDialogBill.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialogBill.Icon")));
            this.printPreviewDialogBill.Name = "printPreviewDialogBill";
            this.printPreviewDialogBill.Visible = false;
            // 
            // printPreviewControlBill
            // 
            this.printPreviewControlBill.Location = new System.Drawing.Point(12, 12);
            this.printPreviewControlBill.Name = "printPreviewControlBill";
            this.printPreviewControlBill.Size = new System.Drawing.Size(735, 368);
            this.printPreviewControlBill.TabIndex = 2;
            // 
            // Bill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(763, 426);
            this.Controls.Add(this.printPreviewControlBill);
            this.Controls.Add(this.Print);
            this.MinimumSize = new System.Drawing.Size(779, 465);
            this.Name = "Bill";
            this.Text = "Bill";
            this.Load += new System.EventHandler(this.Bill_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button Print;
        private System.Drawing.Printing.PrintDocument printDocumentBill;
        private System.Windows.Forms.PrintDialog printDialogBill;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialogBill;
        private System.Windows.Forms.PrintPreviewControl printPreviewControlBill;
    }
}
﻿namespace FormsLibrary
{
    partial class Home
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Registration = new System.Windows.Forms.Button();
            this.Exchange = new System.Windows.Forms.Button();
            this.Transaction = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonLimit = new System.Windows.Forms.Button();
            this.textBoxLimit = new System.Windows.Forms.TextBox();
            this.comboBoxHome = new System.Windows.Forms.ComboBox();
            this.checkBoxCalculate = new System.Windows.Forms.CheckBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.dataGridViewRates = new System.Windows.Forms.DataGridView();
            this.cur1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cur2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rate12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rate21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTipLimit = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRates)).BeginInit();
            this.SuspendLayout();
            // 
            // Registration
            // 
            this.Registration.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Registration.BackColor = System.Drawing.Color.Lavender;
            this.Registration.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.Registration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Registration.Location = new System.Drawing.Point(26, 23);
            this.Registration.Name = "Registration";
            this.Registration.Size = new System.Drawing.Size(135, 37);
            this.Registration.TabIndex = 0;
            this.Registration.Text = "Registration";
            this.Registration.UseVisualStyleBackColor = false;
            this.Registration.Click += new System.EventHandler(this.button1_Click);
            // 
            // Exchange
            // 
            this.Exchange.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Exchange.BackColor = System.Drawing.Color.Lavender;
            this.Exchange.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.Exchange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exchange.Location = new System.Drawing.Point(26, 91);
            this.Exchange.Name = "Exchange";
            this.Exchange.Size = new System.Drawing.Size(135, 37);
            this.Exchange.TabIndex = 1;
            this.Exchange.Text = "Exchange";
            this.Exchange.UseVisualStyleBackColor = false;
            this.Exchange.Click += new System.EventHandler(this.button2_Click);
            // 
            // Transaction
            // 
            this.Transaction.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Transaction.BackColor = System.Drawing.Color.Lavender;
            this.Transaction.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.Transaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Transaction.Location = new System.Drawing.Point(26, 161);
            this.Transaction.Name = "Transaction";
            this.Transaction.Size = new System.Drawing.Size(135, 37);
            this.Transaction.TabIndex = 2;
            this.Transaction.Text = "Transaction";
            this.Transaction.UseVisualStyleBackColor = false;
            this.Transaction.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.Transaction);
            this.groupBox1.Controls.Add(this.Exchange);
            this.groupBox1.Controls.Add(this.Registration);
            this.groupBox1.Location = new System.Drawing.Point(752, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(186, 284);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Thistle;
            this.button2.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(26, 229);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(135, 37);
            this.button2.TabIndex = 3;
            this.button2.Text = "Rest";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Lavender;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(778, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 37);
            this.button1.TabIndex = 7;
            this.button1.Text = "Show Bank Rates";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.checkBoxCalculate);
            this.panel1.Controls.Add(this.buttonSave);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.dataGridViewRates);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(955, 363);
            this.panel1.TabIndex = 8;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonLimit);
            this.groupBox2.Controls.Add(this.textBoxLimit);
            this.groupBox2.Controls.Add(this.comboBoxHome);
            this.groupBox2.Location = new System.Drawing.Point(3, 142);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(138, 202);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Limits";
            // 
            // buttonLimit
            // 
            this.buttonLimit.BackColor = System.Drawing.Color.Lavender;
            this.buttonLimit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonLimit.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonLimit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLimit.Location = new System.Drawing.Point(3, 176);
            this.buttonLimit.Name = "buttonLimit";
            this.buttonLimit.Size = new System.Drawing.Size(132, 23);
            this.buttonLimit.TabIndex = 2;
            this.buttonLimit.Text = "Set Limit";
            this.buttonLimit.UseVisualStyleBackColor = false;
            this.buttonLimit.Click += new System.EventHandler(this.button_Limit_Click);
            // 
            // textBoxLimit
            // 
            this.textBoxLimit.Location = new System.Drawing.Point(8, 19);
            this.textBoxLimit.Name = "textBoxLimit";
            this.textBoxLimit.Size = new System.Drawing.Size(119, 20);
            this.textBoxLimit.TabIndex = 1;
            // 
            // comboBoxHome
            // 
            this.comboBoxHome.FormattingEnabled = true;
            this.comboBoxHome.Location = new System.Drawing.Point(6, 55);
            this.comboBoxHome.Name = "comboBoxHome";
            this.comboBoxHome.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHome.TabIndex = 0;
            this.comboBoxHome.SelectedIndexChanged += new System.EventHandler(this.comboBoxHome_SelectedIndexChanged);
            // 
            // checkBoxCalculate
            // 
            this.checkBoxCalculate.AutoSize = true;
            this.checkBoxCalculate.Location = new System.Drawing.Point(3, 88);
            this.checkBoxCalculate.Name = "checkBoxCalculate";
            this.checkBoxCalculate.Size = new System.Drawing.Size(130, 30);
            this.checkBoxCalculate.TabIndex = 10;
            this.checkBoxCalculate.Text = "Automatic calculation \r\nof exchange rates\r\n";
            this.checkBoxCalculate.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonSave.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.buttonSave.FlatAppearance.BorderSize = 2;
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSave.Location = new System.Drawing.Point(33, 17);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 44);
            this.buttonSave.TabIndex = 9;
            this.buttonSave.Text = "Save All Changes";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // dataGridViewRates
            // 
            this.dataGridViewRates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.dataGridViewRates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRates.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cur1,
            this.cur2,
            this.rate12,
            this.rate21});
            this.dataGridViewRates.Location = new System.Drawing.Point(151, 7);
            this.dataGridViewRates.MultiSelect = false;
            this.dataGridViewRates.Name = "dataGridViewRates";
            this.dataGridViewRates.Size = new System.Drawing.Size(570, 342);
            this.dataGridViewRates.TabIndex = 6;
            this.dataGridViewRates.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridViewRates_CellBeginEdit);
            this.dataGridViewRates.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRates_CellContentClick);
            this.dataGridViewRates.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRates_CellEndEdit);
            this.dataGridViewRates.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewRates_CellFormatting);
            this.dataGridViewRates.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewRates_RowsAdded);
            this.dataGridViewRates.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridViewRates_RowsRemoved);
            // 
            // cur1
            // 
            this.cur1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cur1.HeaderText = "1 currency";
            this.cur1.Name = "cur1";
            this.cur1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cur2
            // 
            this.cur2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cur2.HeaderText = "2 currency";
            this.cur2.Name = "cur2";
            this.cur2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // rate12
            // 
            this.rate12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rate12.HeaderText = "Rate from 1 to 2";
            this.rate12.Name = "rate12";
            this.rate12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // rate21
            // 
            this.rate21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rate21.HeaderText = "Rate from 2 to 1";
            this.rate21.Name = "rate21";
            this.rate21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // toolTipLimit
            // 
            this.toolTipLimit.AutoPopDelay = 5000;
            this.toolTipLimit.BackColor = System.Drawing.Color.Lavender;
            this.toolTipLimit.InitialDelay = 10;
            this.toolTipLimit.ReshowDelay = 20;
            this.toolTipLimit.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTipLimit.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTipLimit_Popup);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(971, 387);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(616, 426);
            this.Name = "Home";
            this.Text = "Home";
            this.Load += new System.EventHandler(this.Home_Load_1);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Home_KeyUp);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRates)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Registration;
        private System.Windows.Forms.Button Exchange;
        private System.Windows.Forms.Button Transaction;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridViewRates;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.CheckBox checkBoxCalculate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonLimit;
        private System.Windows.Forms.TextBox textBoxLimit;
        private System.Windows.Forms.ComboBox comboBoxHome;
        private System.Windows.Forms.ToolTip toolTipLimit;
        private System.Windows.Forms.DataGridViewTextBoxColumn cur1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cur2;
        private System.Windows.Forms.DataGridViewTextBoxColumn rate12;
        private System.Windows.Forms.DataGridViewTextBoxColumn rate21;
    }
}


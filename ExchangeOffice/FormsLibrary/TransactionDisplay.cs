﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibraryEO;
using ModulesLibrary;


namespace FormsLibrary
{
    public partial class TransactionDisplay : Form
    {
        /*TransactionsRepository transactions;
        RateRepository rates;*/
        private DataContext data;
        Boolean isInitialized = false;
        private List<Control> filters;
        Boolean isFilteredByFrom = false;
        Boolean isFilteredByTo = false;

        //BindingList<Transaction> bindingListTransaction;
        public TransactionDisplay()
        {
            /*transactions = new TransactionsRepository();
            rates = new RateRepository();   */        
            InitializeComponent();
            data = new DataContext();
            TFrom.DataSource = data.rates.GetCurrencies();
            TTo.DataSource = data.rates.GetCurrencies();
            filters = new List<Control>() { TFrom, TTo, TId, dateTimePicker1, dateTimePicker2 };
            isInitialized = true;
            PaintGrid();
        }
        public void PaintGrid()
        {
            // bindingListTransaction = new BindingList<Transaction>(transactions.transactions);
           // if(transactions.transactions.Count==0) MessageBox.Show("Ничего не найдено");
            dataGridViewTransactions.Rows.Clear();
            foreach (Transaction tr in data.transactions)
            {
                dataGridViewTransactions.Rows.Add(tr.fromCurrency, tr.toCurrency, tr.amountFromCurrency.ToString(), tr.rate, tr.amountToCurrency, tr.id, tr.dateTime.ToString(), tr.operationNumber);
            }
            if (isFilteredByFrom && isFilteredByTo)
            {
                dataGridViewTransactions.Rows.Add("Итого:", "", data.transactions.CalculateVolumeFrom(), "", data.transactions.CalculateVolumeTo());
            }
            else
            if (isFilteredByFrom)
            {
                dataGridViewTransactions.Rows.Add("Итого:", "", data.transactions.CalculateVolumeFrom());
            }
            else
            if (isFilteredByTo)
            {
                dataGridViewTransactions.Rows.Add("Итого:", "", "", "", data.transactions.CalculateVolumeTo());
            }
            
            
            
          
            dataGridViewTransactions.Update();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTime a = dateTimePicker1.Value;

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void TransactionDisplay_Load(object sender, EventArgs e)
        {

        }

        private void Reset_Click(object sender, EventArgs e)
        {
            isFilteredByFrom = false;
            isFilteredByTo = false;
            foreach(var f in filters)
            {
                f.ResetText();
            }
            data.transactions.ResetFilters();
            
            PaintGrid();
        }

        private void TFrom_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged_1(object sender, EventArgs e)
        {
            data.transactions.FilterByDate(dateTimePicker1.Value, dateTimePicker2.MaxDate);
            PaintGrid();
           // MessageBox.Show(transactions.transactions.Count.ToString()); 
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            data.transactions.FilterByDate(dateTimePicker1.MinDate, dateTimePicker2.Value);
            PaintGrid();
           // MessageBox.Show(transactions.transactions.Count.ToString());
        }

        private void TFrom_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (isInitialized)
            {
                isFilteredByFrom = true;
                data.transactions.FilterByFromCurrency(TFrom.Text);
                PaintGrid();
            }
            
        }

        private void TTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isInitialized)
            {
                isFilteredByTo = true;
                data.transactions.FilterByToCurrency(TTo.Text);
                PaintGrid();
            }
        }

        private void TId_TextChanged(object sender, EventArgs e)
        {
            //transactions.FilterById(TId.Text);
            //PaintGrid();
        }

        private void SetId_Click(object sender, EventArgs e)
        {
            data.transactions.FilterById(TId.Text);
            PaintGrid();
        }
    }
}

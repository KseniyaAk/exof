﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibraryEO;
using ModulesLibrary;

namespace FormsLibrary
{
    public partial class Registration : Form
    {
        /*UserRepository users;
        Exchange exchange;*/
        private DataContext data;
        private List<Control> controlElements;

        public Registration()
        {
            InitializeComponent();
            data = new DataContext();
            controlElements = new List<Control>() { RegId, RegName, RegSurname };

        }
        public Registration(String id)
        {
            InitializeComponent();
            data = new DataContext();
            RegId.Text = id;
            RegId.Enabled = false;
            controlElements = new List<Control>() { RegId, RegName, RegSurname };
        }

        private void SetUser_Click(object sender, EventArgs e)
        {
            DialogResult result;
            if(RegName.Text == "" || RegSurname.Text == "" || RegId.Text == "")
            {
                MessageBox.Show("Не все поля формы заполнены");
                return;
            }            
            User user;

            try
            {
                user = new User(RegName.Text, RegSurname.Text, RegId.Text);
                if (data.users.Find(RegName.Text, RegSurname.Text) != null)
                {
                    result = MessageBox.Show("Клиент с ведённым ФИО уже существует \nВы уверены, что правильно ввели ID", "Упс", MessageBoxButtons.YesNo);
                    if (result == DialogResult.No)
                    {
                        RegId.Text = "";
                        return;

                        /* if (exchange != null)
                         {
                             //exchange.ExMutex.Set();
                         }*/
                    }
                   
                }
                data.Add(user);
                MessageBox.Show("Успешно");
                ResetTextBox();

            }
            catch (UserAlreadyExistExeption) { MessageBox.Show("Клиент с ведённым ID уже существует"); ResetTextBox(); }
            catch (FormatException) { MessageBox.Show("Проверте введённые данные", "Ошибка формата"); }           
            
        }
        private void ResetTextBox()
        {
            foreach(var e in controlElements)
            {
                e.Text = "";
            }         

        }

        private void Registration_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void RegSurname_TextChanged(object sender, EventArgs e)
        {

        }        
    }
}

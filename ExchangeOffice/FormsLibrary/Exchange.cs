﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModulesLibrary;
using ClassLibraryEO;

namespace FormsLibrary
{
    public partial class Exchange : Form
    {
        private DataContext data;
        private List<Control> controlElements;
        /*private UserRepository users;

        private TransactionsRepository transactions;
        private RateRepository rates;
        private LimitSevices limits;*/
        //public AutoResetEvent ExMutex;

        public Exchange()
        {
            InitializeComponent();
            //ExMutex = new AutoResetEvent(false);
            data = new DataContext();
            ExFrom.DataSource = data.rates.GetCurrencies();
            ExTo.DataSource = data.rates.GetCurrencies();
            controlElements = new List<Control>() { ExAmount, ExId, ExName, ExSurname, ExFrom, ExTo };
        }

        private void button_Exchange_Click(object sender, EventArgs e)
        {
            DialogResult result;
            if (ExId.Text == "" || ExFrom.Text == "" || ExTo.Text == "" || ExAmount.Text == "")
            {
                MessageBox.Show("Не все поля формы заполнены", "Упс", MessageBoxButtons.YesNo);
                return;
            }

            try
            {
                User user = data.users.Find(ExId.Text);
                Double rate = data.rates.Find(ExFrom.Text, ExTo.Text);
                Transaction newTransaction = new Transaction(ExFrom.Text, ExTo.Text, Double.Parse(ExAmount.Text), rate, ExId.Text);
                result = MessageBox.Show("Подтвеждаете транзакцию?", "", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    data.Add(newTransaction);
                    ResetForm();
                    new Bill(user, newTransaction).ShowDialog();
                }
            }
            catch (UserNotFoundException)
            {
                result = MessageBox.Show("Клиент банка не найден \nХотите зарегистрировать клиента?", "Упс", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    new Registration(ExId.Text).ShowDialog();
                }
                data.users.Updata();
            }
            catch (ExchangeRateNotFoundException)
            {
                MessageBox.Show("Не можем найти курс \nПроверте, задана ли данная конверсия", "Упс");
            }
            catch (LimitExcessExeption)
            {
                MessageBox.Show("К сожалению лимит превышен");
            }
            catch (FormatException)
            {
                MessageBox.Show("Проверте введённые данные", "Ошибка формата");

            }
            catch (LimitNotFoundException)
            {
                MessageBox.Show("Не можем найти лимит для одной из валют", "Упс");
            }
        }
        //private List<Control> controlElements; 
        private void ResetForm()
        {
            foreach(var el in controlElements)
            {
                el.ResetText();
            }
        }
        private void ExId_TextChanged(object sender, EventArgs e)
        {
            User user;
            try
            {
                user = data.users.Find(ExId.Text);
                ExName.Text = user.name;
                ExSurname.Text = user.surname;
            }
            catch(UserNotFoundException)
            {
                ExName.Text = null;
                ExSurname.Text = null;
            }
        }  
       /* private void button_Exchange_Click(object sender, EventArgs e)
        {
            if (ExId.Text == "" || ExFrom.Text == "" || ExTo.Text == "" || ExAmount.Text == "")
            {
                MessageBox.Show("Заполните все поля", "Упс");
                return;
            }
            try
            {
                User user = data.users.Find(ExId.Text);
                Double rate = data.rates.Find(ExFrom.Text, ExTo.Text);

            } catch (UserNotRegistratedException)


            DialogResult result;


            {
                if (rate == 0) MessageBox.Show("Не можем найти курс \nПроверте, задана ли данная конверсия", "Упс");
                else
                {
                    if (user == null)
                    {
                        result = MessageBox.Show("Клиент банка не найден \nХотите зарегистрировать клиента?", "Упс", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            for (; ; )
                            {*/
                                /* new Thread(new ThreadStart(StartRegistration)).Start(); don't mind. I can not remove this masterpiece
                                
                                 ExMutex.WaitOne();*/
                                /*new Registration(this).ShowDialog();
                                users.Update();
                                if (users.Find(ExId.Text) == null)
                                {
                                    result = MessageBox.Show("Регистрация прошла неудачно \nХотите зарегистрировать клиента?", "Упс", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.Yes) continue;
                                    else break;
                                }
                                else
                                {
                                    result = MessageBox.Show("Подтвеждаете транзакцию?", "Точно?", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.Yes)
                                    {
                                        transactions.Add(new Transaction(ExFrom.Text, ExTo.Text, Double.Parse(ExAmount.Text), rate, ExId.Text));
                                        ResetForm();
                                        new Bill().Show();
                                    }

                                    break;
                                }
                            }

                        }
                    } else
                    {
                        Transaction newTransaction = new Transaction(ExFrom.Text, ExTo.Text, Double.Parse(ExAmount.Text), rate, ExId.Text);
                        if (transactions.LimitCheck(newTransaction, limits.GetLimit(ExFrom.Text), limits.GetLimit(ExTo.Text)))
                        {
                            result = MessageBox.Show("Подтвеждаете транзакцию?", "", MessageBoxButtons.YesNo);
                            if (result == DialogResult.Yes)
                            {
                                transactions.Add(newTransaction);
                                ResetForm();
                                new Bill().Show();
                            }
                        }
                        else
                        {
                            MessageBox.Show("К сожалению лимит превышен");
                        }
                    }
                }
            }
           }
        }*/

       

        /*private void StartRegistration()
        {
            new Registration(this).ShowDialog();
        }*/
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Exchange_Load(object sender, EventArgs e)
        {

        }

        private void Surname_TextChanged(object sender, EventArgs e)
        {

        }

        

        private void ExTimer_Tick(object sender, EventArgs e)
        {

        }

        private void Exchange_Load_1(object sender, EventArgs e)
        {

        }
    }
    }

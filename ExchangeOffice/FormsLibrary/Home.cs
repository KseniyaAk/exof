﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibraryEO;
using ModulesLibrary;

namespace FormsLibrary
{

    public partial class Home : Form    {

        private DataContext data;
       /* public RateRepository rates;
        public LimitSevices limits;*/
        
        //  Boolean isCTRLpressed = false;
        //  Boolean isFirstChange = true;
        Boolean newRowAdded = false;
        Boolean clearWork = false;
        List<Int32> deletedRowsNumbers;
        
        public Home()
        {
            InitializeComponent();
            /*rates = new RateRepository();
            limits = new LimitSevices();*/
            data = new DataContext();           
            deletedRowsNumbers = new List<int>();
            KeyPreview = true;
            //toolTipLimit.UseAnimation = true;
            comboBoxHome.DataSource = data.rates.GetCurrencies();            
            PaintGrid();
        }

        private void PaintGrid()
        {
            clearWork = true;
            dataGridViewRates.Rows.Clear();
            clearWork = false;

            Int32 n = 0;

            foreach (Rate r in data.rates)
            {
                if( !deletedRowsNumbers.Contains(n++) )
                dataGridViewRates.Rows.Add(r.nameFromCurrency, r.nameToCurrency, r.rateTo, r.rateFrom);               
            }               

            newRowAdded = false;
            //dataGridViewRates.ReadOnly = true;
        }

        
        private void dataGridViewRates_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            data.rates.Push(e.RowIndex);                 
        }      
        

        private void dataGridViewRates_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Rate newRate=null;           
            try {
                newRate = new Rate(dataGridViewRates[0, e.RowIndex].Value.ToString(), dataGridViewRates[1, e.RowIndex].Value.ToString(),
                    Double.Parse(dataGridViewRates[2, e.RowIndex].Value.ToString()), Double.Parse(dataGridViewRates[3, e.RowIndex].Value.ToString()));

                if (newRowAdded) { data.Add(newRate, checkBoxCalculate.Checked, e.ColumnIndex); newRowAdded = false; }
                else data.Add(newRate, e.RowIndex, checkBoxCalculate.Checked, e.ColumnIndex);
            }
            catch(FormatException)
            {
                MessageBox.Show("Проверте введённые данные", "Ошибка формата");                
            }
            catch(NullReferenceException) { }
            catch (ExchangeRateAlreadyExistException) { MessageBox.Show("Данная конверсия уже задана"); }
           
            comboBoxHome.DataSource = data.rates.GetCurrencies();             
        }

        private void button_Save_Click(object sender, EventArgs e)
        {            
            data.rates.Save(deletedRowsNumbers);
            PaintGrid();
        }

        //never delete
        private void Home_KeyUp(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Z && e.Modifiers == Keys.Control && deletedRowsNumbers.Count > 0)
            {//MessageBox.Show("миракел");
                deletedRowsNumbers.RemoveAt(deletedRowsNumbers.Count-1);
                data.rates.Pop();
                PaintGrid();
            }
        }

        private void dataGridViewRates_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            newRowAdded = true;
        }

        private void button_Limit_Click(object sender, EventArgs e)
        {
            Limit limit;
            try
            {
                limit = new Limit(comboBoxHome.Text, Double.Parse(textBoxLimit.Text));
                data.Set(limit);
                MessageBox.Show("Удачно");

            } catch(FormatException)
            {
                MessageBox.Show("Проверте введённые данные","Ошибка формата");
                limit = null;
            }catch(LimitNotFoundException) { MessageBox.Show("Проверте введённые данные", "Валюта не найдена"); }

            ResetLimits();
            
            
        }
        private void ResetLimits()
        {
            textBoxLimit.Text = "";
        }

        private void dataGridViewRates_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewCell cell = dataGridViewRates.Rows[e.RowIndex].Cells[e.ColumnIndex];
            try
            {
                if (e.ColumnIndex < 2 && e.Value != null)
                {
                    
                    cell.ToolTipText = data.limits.Find(cell.Value.ToString()).ToString();
                }
            }
            catch (LimitNotFoundException) { cell.ToolTipText = "Не установлен"; }
            
        }
        private void dataGridViewRates_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (!clearWork)
            {
                data.rates.Push(e.RowIndex);
                deletedRowsNumbers.Add(e.RowIndex);

            }
            
        }
        /*private void dataGridViewRates_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //dataGridViewRates.ReadOnly = true;
        }        
        private void Home_KeyDown(object sender, KeyEventArgs e)
        {            
            

        }

        private void Home_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Control) isCTRLpressed = false;
        }
        
        private void dataGridViewRates_SelectionChanged(object sender, EventArgs e)
        {
            //isFirstChange = true;
        }

        private void Home_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 162) isCTRLpressed = true;
        }
    
        private void dataGridViewRates_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void dataGridViewRates_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            isFirstChange = true;
        }

        private void Home_KeyUp(object sender, KeyEventArgs e)
        {
            
        }*/

        /*public void DataGridView_CellEventHandler(object sender, DataGridViewCellEventArgs e)
        {
            Panel panel = new Panel();
            TextBox textBox = new TextBox();
            textBox.Text = dataGridViewRates[e.ColumnIndex, e.RowIndex].Value.ToString();
            Button button = new Button();
            button.Text = "Change";
            panel.Controls.Add(textBox);
            panel.Controls.Add(button);
            dataGridViewRates[e.ColumnIndex, e.RowIndex].Value = panel;

        }  */ 

        private void button1_Click(object sender, EventArgs e)
        {
            new Registration().Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new Exchange().Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new TransactionDisplay().Show();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            new Rest().Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            new Bank().ShowDialog();
        }

        //never delete
        private void Home_Load_1(object sender, EventArgs e) { }

        private void toolTipLimit_Popup(object sender, PopupEventArgs e) { }

        private void dataGridViewRates_CellContentClick(object sender, DataGridViewCellEventArgs e) { }

        private void groupBox1_Enter(object sender, EventArgs e) { }

        private void Home_Load(object sender, EventArgs e) { }

        private void comboBoxHome_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

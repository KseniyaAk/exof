﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibraryEO;
using ModulesLibrary;

namespace FormsLibrary
{
    public partial class Bill : Form
    {
        public Bill(User user, Transaction transaction)
        {
            InitializeComponent();
            BillService.WriteBill(user, transaction);
            printDocumentBill.DocumentName = BillService.GetFileName();
            printPreviewControlBill.Document = printDocumentBill;
            printPreviewDialogBill.Document = printDocumentBill;
           


        }

        private void Bill_Load(object sender, EventArgs e)
        {
            
        }

        private void Print_Click(object sender, EventArgs e)
        {
            printPreviewDialogBill.ShowDialog();
        }
    }
}

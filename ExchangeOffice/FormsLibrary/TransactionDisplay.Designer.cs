﻿namespace FormsLibrary
{
    partial class TransactionDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Filters = new System.Windows.Forms.GroupBox();
            this.SetId = new System.Windows.Forms.Button();
            this.ExToDate = new System.Windows.Forms.Label();
            this.ExFromDate = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.TTo = new System.Windows.Forms.ComboBox();
            this.TFrom = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Reset = new System.Windows.Forms.Button();
            this.TId = new System.Windows.Forms.TextBox();
            this.dataGridViewTransactions = new System.Windows.Forms.DataGridView();
            this.ColumnFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OperationNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Filters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTransactions)).BeginInit();
            this.SuspendLayout();
            // 
            // Filters
            // 
            this.Filters.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Filters.Controls.Add(this.SetId);
            this.Filters.Controls.Add(this.ExToDate);
            this.Filters.Controls.Add(this.ExFromDate);
            this.Filters.Controls.Add(this.dateTimePicker2);
            this.Filters.Controls.Add(this.dateTimePicker1);
            this.Filters.Controls.Add(this.TTo);
            this.Filters.Controls.Add(this.TFrom);
            this.Filters.Controls.Add(this.label3);
            this.Filters.Controls.Add(this.label2);
            this.Filters.Controls.Add(this.label1);
            this.Filters.Controls.Add(this.Reset);
            this.Filters.Controls.Add(this.TId);
            this.Filters.Location = new System.Drawing.Point(14, 12);
            this.Filters.Name = "Filters";
            this.Filters.Size = new System.Drawing.Size(214, 380);
            this.Filters.TabIndex = 1;
            this.Filters.TabStop = false;
            this.Filters.Text = "Filters";
            // 
            // SetId
            // 
            this.SetId.BackColor = System.Drawing.Color.Lavender;
            this.SetId.Location = new System.Drawing.Point(164, 286);
            this.SetId.Name = "SetId";
            this.SetId.Size = new System.Drawing.Size(44, 23);
            this.SetId.TabIndex = 14;
            this.SetId.Text = "Set";
            this.SetId.UseVisualStyleBackColor = false;
            this.SetId.Click += new System.EventHandler(this.SetId_Click);
            // 
            // ExToDate
            // 
            this.ExToDate.AutoSize = true;
            this.ExToDate.Location = new System.Drawing.Point(98, 116);
            this.ExToDate.Name = "ExToDate";
            this.ExToDate.Size = new System.Drawing.Size(20, 13);
            this.ExToDate.TabIndex = 13;
            this.ExToDate.Text = "To";
            // 
            // ExFromDate
            // 
            this.ExFromDate.AutoSize = true;
            this.ExFromDate.Location = new System.Drawing.Point(98, 20);
            this.ExFromDate.Name = "ExFromDate";
            this.ExFromDate.Size = new System.Drawing.Size(30, 13);
            this.ExFromDate.TabIndex = 12;
            this.ExFromDate.Text = "From";
            this.ExFromDate.Click += new System.EventHandler(this.label4_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(6, 145);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 11;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(6, 48);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 10;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged_1);
            // 
            // TTo
            // 
            this.TTo.FormattingEnabled = true;
            this.TTo.Items.AddRange(new object[] {
            "",
            "BYN",
            "EURO"});
            this.TTo.Location = new System.Drawing.Point(135, 219);
            this.TTo.Name = "TTo";
            this.TTo.Size = new System.Drawing.Size(69, 21);
            this.TTo.TabIndex = 9;
            this.TTo.SelectedIndexChanged += new System.EventHandler(this.TTo_SelectedIndexChanged);
            // 
            // TFrom
            // 
            this.TFrom.FormattingEnabled = true;
            this.TFrom.Items.AddRange(new object[] {
            "",
            "BYN",
            "EURO"});
            this.TFrom.Location = new System.Drawing.Point(42, 219);
            this.TFrom.Name = "TFrom";
            this.TFrom.Size = new System.Drawing.Size(64, 21);
            this.TFrom.TabIndex = 8;
            this.TFrom.SelectedIndexChanged += new System.EventHandler(this.TFrom_SelectedIndexChanged_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 295);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(109, 227);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "To";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 227);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "From";
            // 
            // Reset
            // 
            this.Reset.BackColor = System.Drawing.Color.Lavender;
            this.Reset.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Reset.Location = new System.Drawing.Point(3, 342);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(208, 35);
            this.Reset.TabIndex = 4;
            this.Reset.Text = "Updata";
            this.Reset.UseVisualStyleBackColor = false;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // TId
            // 
            this.TId.Location = new System.Drawing.Point(42, 288);
            this.TId.Name = "TId";
            this.TId.Size = new System.Drawing.Size(116, 20);
            this.TId.TabIndex = 3;
            this.TId.TextChanged += new System.EventHandler(this.TId_TextChanged);
            // 
            // dataGridViewTransactions
            // 
            this.dataGridViewTransactions.AllowUserToAddRows = false;
            this.dataGridViewTransactions.AllowUserToDeleteRows = false;
            this.dataGridViewTransactions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.dataGridViewTransactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTransactions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnFrom,
            this.ColumnTo,
            this.ColumnAmount,
            this.Rate,
            this.AmountTo,
            this.ColumnId,
            this.ColumnDate,
            this.OperationNumber});
            this.dataGridViewTransactions.Location = new System.Drawing.Point(235, 22);
            this.dataGridViewTransactions.Name = "dataGridViewTransactions";
            this.dataGridViewTransactions.ReadOnly = true;
            this.dataGridViewTransactions.Size = new System.Drawing.Size(735, 370);
            this.dataGridViewTransactions.TabIndex = 2;
            // 
            // ColumnFrom
            // 
            this.ColumnFrom.HeaderText = "From";
            this.ColumnFrom.Name = "ColumnFrom";
            this.ColumnFrom.ReadOnly = true;
            this.ColumnFrom.Width = 80;
            // 
            // ColumnTo
            // 
            this.ColumnTo.HeaderText = "To";
            this.ColumnTo.Name = "ColumnTo";
            this.ColumnTo.ReadOnly = true;
            this.ColumnTo.Width = 80;
            // 
            // ColumnAmount
            // 
            this.ColumnAmount.HeaderText = "Amount From";
            this.ColumnAmount.Name = "ColumnAmount";
            this.ColumnAmount.ReadOnly = true;
            // 
            // Rate
            // 
            this.Rate.HeaderText = "Rate";
            this.Rate.Name = "Rate";
            this.Rate.ReadOnly = true;
            // 
            // AmountTo
            // 
            this.AmountTo.HeaderText = "Amount To";
            this.AmountTo.Name = "AmountTo";
            this.AmountTo.ReadOnly = true;
            // 
            // ColumnId
            // 
            this.ColumnId.HeaderText = "ID";
            this.ColumnId.Name = "ColumnId";
            this.ColumnId.ReadOnly = true;
            // 
            // ColumnDate
            // 
            this.ColumnDate.HeaderText = "Date";
            this.ColumnDate.Name = "ColumnDate";
            this.ColumnDate.ReadOnly = true;
            this.ColumnDate.Width = 130;
            // 
            // OperationNumber
            // 
            this.OperationNumber.HeaderText = "Operation Number";
            this.OperationNumber.Name = "OperationNumber";
            this.OperationNumber.ReadOnly = true;
            // 
            // TransactionDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(982, 405);
            this.Controls.Add(this.dataGridViewTransactions);
            this.Controls.Add(this.Filters);
            this.MinimumSize = new System.Drawing.Size(817, 444);
            this.Name = "TransactionDisplay";
            this.Text = " ";
            this.Load += new System.EventHandler(this.TransactionDisplay_Load);
            this.Filters.ResumeLayout(false);
            this.Filters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTransactions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox Filters;
        private System.Windows.Forms.TextBox TId;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox TTo;
        private System.Windows.Forms.ComboBox TFrom;
        private System.Windows.Forms.Label ExFromDate;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label ExToDate;
        private System.Windows.Forms.DataGridView dataGridViewTransactions;
        private System.Windows.Forms.Button SetId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperationNumber;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModulesLibrary
{
    public class Limit
    {
        public String nameOfCurrency { get; set; }
        public Double limit { get; set; }

        public Limit(string nameOfCurrency, double limit)
        {
            this.nameOfCurrency = nameOfCurrency;
            this.limit = limit;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModulesLibrary
{
    public class Transaction
    {
        public String fromCurrency { get; set; }
        public String toCurrency { get; set; }
        public Double amountFromCurrency { get; set; }
        public Double amountToCurrency { get; set; }
        public Double rate { get; set; }
        public DateTime dateTime { get; set; }
        public String id { get; set; }
        public Int64 operationNumber { get; set; } = 0;

        public Transaction(string fromCurrency, string toCurrency, double amountFromCurrency, double rate, string id)
        {
            this.fromCurrency = fromCurrency;
            this.toCurrency = toCurrency;
            this.amountFromCurrency = amountFromCurrency;
            this.rate = rate;
            this.id = id;
            dateTime = DateTime.Now;
            amountToCurrency = amountFromCurrency * rate;
        }

        public Transaction(string fromCurrency, string toCurrency, double amountFromCurrency, double amountToCurrency, double rate, DateTime dateTime, string id, Int64 operationNumber)
        {
            this.fromCurrency = fromCurrency;
            this.toCurrency = toCurrency;
            this.amountFromCurrency = amountFromCurrency;
            this.amountToCurrency = amountToCurrency;
            this.rate = rate;
            this.dateTime = dateTime;
            this.id = id;
            this.operationNumber = operationNumber;
        }
    }
}

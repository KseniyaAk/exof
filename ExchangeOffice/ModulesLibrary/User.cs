﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModulesLibrary
{
    public class User
    {
        public string name { get; }
        public string surname { get; }
        public string id { get; }
         
        public User(string name, string surname, string id) {
            this.name = name;
            this.surname = surname;
            this.id = id;
        }

        
    }
}

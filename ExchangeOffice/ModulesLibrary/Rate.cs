﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModulesLibrary
{
    public class Rate
    {
        public String nameFromCurrency { get; set; }
        public String nameToCurrency { get; set; }
        public Double rateTo { get; set; }
        public Double rateFrom { get; set; }

        public Rate(string nameFromCurrency, string nameToCurrency, double rateTo, double rateFrom)
        {
            this.nameFromCurrency = nameFromCurrency;
            this.nameToCurrency = nameToCurrency;
            this.rateTo = rateTo;
            this.rateFrom = rateFrom;

            //if (rateTo == -1) this.rateTo = rateFrom * 1.10;
            //if (rateFrom == -1) this.rateFrom = rateTo * 0.9;
        }

    }
}

﻿using ModulesLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace DataBaseAccess
{
    public class TransactionsAccess
    {
        private const String nameXML = @"D:\1 университет\5 семестр\Прога\Проект\ExchangeOffice\DataBaseAccess\TransactionsXML\transactions.xml";
        private static XDocument xmlDocument;

        static TransactionsAccess()
        {
            xmlDocument = new XDocument();
            xmlDocument = XDocument.Load(nameXML);
        }

        public static List<Transaction> GetData()
        {
            List<Transaction> transactions = new List<Transaction>();
            List<Transaction> sortedTransactions;

            foreach (XElement t in xmlDocument.Element("transactions").Elements("transaction"))
            {
                transactions.Add(new Transaction( t.Element("nameFromCurrency").Value, t.Element("nameToCurrency").Value, 
                    Double.Parse(t.Element("amountFromCurrency").Value), Double.Parse(t.Element("amountToCurrency").Value),
                     Double.Parse(t.Element("rate").Value), DateTime.Parse(t.Element("dateTime").Value), t.Attribute("id").Value,
                     Int64.Parse(t.Element("operationNumber").Value)));
            }

            //sort transactions
            sortedTransactions = new List<Transaction>(from t in transactions orderby t.dateTime select t);
            return sortedTransactions;
        }
        public static void Add(Transaction transaction)
        {
            XElement xTransaction = new XElement("transaction");
            xTransaction.Add( new XAttribute("id",transaction.id),
                new XElement("nameFromCurrency", transaction.fromCurrency),
                new XElement("nameToCurrency", transaction.toCurrency),
                new XElement("amountFromCurrency", transaction.amountFromCurrency.ToString()),
                new XElement("rate", transaction.rate.ToString()),
                new XElement("amountToCurrency", transaction.amountToCurrency.ToString()),
                new XElement("dateTime", transaction.dateTime.ToString()),
                new XElement("operationNumber", transaction.operationNumber.ToString()) );

            xmlDocument.Element("transactions").Add(xTransaction);

            xmlDocument.Save(nameXML);
        }
    }
}

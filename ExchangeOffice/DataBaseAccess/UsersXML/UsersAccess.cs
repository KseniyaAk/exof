﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModulesLibrary;
using System.Xml.Linq;

namespace DataBaseAccess
{
    public class UsersAccess
    {
        private const String nameXML = @"D:\1 университет\5 семестр\Прога\Проект\ExchangeOffice\DataBaseAccess\UsersXML\users.xml";
        private static XDocument xmlDocument;

        static UsersAccess()
        {
            xmlDocument = new XDocument();
            xmlDocument = XDocument.Load(nameXML);
        }

        public static List<User> GetData()
        {
            List<User> users = new List<User>();
            List<User> sortedUsers;

            foreach (XElement u in xmlDocument.Element("users").Elements("user"))
            {
                users.Add(new User(u.Element("name").Value, u.Element("surname").Value, u.Attribute("id").Value));
            }

            //sort users
            sortedUsers= new List<User>(users.OrderBy(u=>u.surname));
            return sortedUsers;
        }
        public static void Add(User user)
        {
            XElement xUser = new XElement("user");
            xUser.Add(new XAttribute("id", user.id),
                new XElement("name", user.name),
                new XElement("surname", user.surname));

            xmlDocument.Element("users").Add(xUser);

            xmlDocument.Save(nameXML);
        }
    }
}

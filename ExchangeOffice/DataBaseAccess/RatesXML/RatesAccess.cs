﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModulesLibrary;
using System.Xml;
using System.Xml.Linq;


namespace DataBaseAccess
{
    public class RatesAccess
    {
        private const String nameXML = @"D:\1 университет\5 семестр\Прога\Проект\ExchangeOffice\DataBaseAccess\RatesXML\rates.xml";
        private static XDocument xmlDocument;

        static RatesAccess()
        {
            xmlDocument = new XDocument();
            xmlDocument =XDocument.Load(nameXML);
        }

        public static List<Rate> GetData()
        {
            List<Rate> rates = new List<Rate>();
            List<Rate> sortedRates = new List<Rate>();

            foreach (XElement r in xmlDocument.Element("rates").Elements("rate"))
            {
                rates.Add( new Rate(r.Attribute("nameFromCurrency").Value, r.Attribute("nameToCurrency").Value, 
                    Double.Parse(r.Element("rateFrom").Value), Double.Parse(r.Element("rateTo").Value)) );
            }

            //sort rates
            List<Rate> ratesBYN = new List<Rate>(from r in rates where r.nameFromCurrency == "BYN" select r);
            ratesBYN.AddRange(from r in rates where r.nameToCurrency == "BYN" select r);
            sortedRates = ratesBYN;
            sortedRates.AddRange(rates.Except(ratesBYN));
            return sortedRates;
        }
        public static void Add(Rate rate)
        {
            XElement xRate = new XElement("rate");
            xRate.Add(new XAttribute("nameFromCurrency",rate.nameFromCurrency), 
                new XAttribute("nameToCurrency",rate.nameToCurrency), 
                new XElement("rateFrom",rate.rateFrom.ToString()), 
                new XElement("rateTo",rate.rateTo.ToString()));

            xmlDocument.Element("rates").Add(xRate);

            xmlDocument.Save(nameXML);
        }
        public static void Clear()
        {
            xmlDocument.Element("rates").RemoveNodes();
            xmlDocument.Save(nameXML);
        }
        public static void AddRange(List<Rate> newRates)
        {
            foreach(var r in newRates)
            {
                Add(r);
            }
        }
    }
}

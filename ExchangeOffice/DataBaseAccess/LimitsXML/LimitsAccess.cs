﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ModulesLibrary;

namespace DataBaseAccess
{
    public class LimitsAccess
    {
        private const String nameXML = @"D:\1 университет\5 семестр\Прога\Проект\ExchangeOffice\DataBaseAccess\LimitsXML\limits.xml";
        private static XDocument xmlDocument;

        static LimitsAccess()
        {
            xmlDocument = new XDocument();
            xmlDocument = XDocument.Load(nameXML);
        }

        public static List<Limit> GetData()
        {
            List<Limit> limits = new List<Limit>();
            List<Limit> sortedLimits = new List<Limit>();            

            foreach (XElement r in xmlDocument.Element("limits").Elements("limit"))
            {
                limits.Add(new Limit(r.Attribute("nameOfCurrency").Value, Double.Parse(r.Element("limit").Value)));
            }

            //sort limits            
            sortedLimits = new List<Limit>(limits.OrderBy(l=>l.nameOfCurrency));
            return sortedLimits;
        }
        public static void Add(Limit limit)
        {
            XElement xLimit = new XElement("limit");
            xLimit.Add(new XAttribute("nameOfCurrency", limit.nameOfCurrency),                
                new XElement("limit", limit.limit));

            xmlDocument.Element("limits").Add(xLimit);

            xmlDocument.Save(nameXML);
        }
        public static void Clear()
        {
            xmlDocument.Element("limits").RemoveNodes();
            xmlDocument.Save(nameXML);
        }
        public static void AddRange(List<Limit> newLimits)
        {
            foreach (var r in newLimits)
            {
                Add(r);
            }
        }
    }
}
